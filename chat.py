#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021 Cebtenzzre
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import argparse
import ctypes
import curses
import errno
import json
import os
import queue
import signal
import socket
import sys
import threading
import traceback
from collections import deque
from dataclasses import dataclass
from functools import partial
from itertools import count
from json import JSONDecodeError
from typing import TYPE_CHECKING, Any, Generic, Optional, TypeVar, overload

if TYPE_CHECKING:
    from curses import _CursesWindow
    from signal import Signals
    from threading import Lock, _RLock
    from types import FrameType, TracebackType
    from typing import (Callable, Container, Deque, Dict, Iterator, List, Literal, MutableSequence, NoReturn, Sequence,
                        Tuple, Type, Union)

    ExcInfo = Tuple[Type[BaseException], BaseException, TracebackType]
    OptExcInfo = Union[ExcInfo, Tuple[None, None, None]]
    AnyLock = Union[Lock, _RLock]

    WaiterSeq = Deque[Any]
    WaiterPair = Tuple[AnyLock, Sequence[Any]]

    T = TypeVar('T')
else:
    WaiterSeq = deque

if os.name == 'posix':
    import readline  # For message/command history  # noqa: F401
    try:
        import termios
    except ImportError:
        termios = None  # type: ignore[assignment]
else:  # os.name == 'nt'
    from ctypes import wintypes
    termios = None  # type: ignore[assignment]

Response = TypeVar('Response')


MAX_MESSAGES = 200  # Comfortably large in case of tiny terminal font
COLORS: Tuple[Tuple[str, int, int], ...] = (
    ('status', curses.COLOR_YELLOW, -1),
    ('me', curses.COLOR_MAGENTA, -1),
)
global have_colors
have_colors = False  # Set to true if color is available


class CursesState:
    def __init__(self, screen: _CursesWindow) -> None:
        self.screen = screen
        self.color_pair_idx = 1
        self.color_pairs: Dict[str, int] = {}
        self.input_enabled = False
        self.input_buf = ''
        self.input_pos = 0
        self.scroll_pos = 0
        self.messages: Deque[Dict[str, Any]] = deque(maxlen=MAX_MESSAGES)
        self.ycur = 0  # Placeholder
        self.resetpos()

    def make_color_pair(self, name: str, fg: int, bg: int) -> None:
        global have_colors
        if not have_colors:
            return  # No color support
        curses.init_pair(self.color_pair_idx, fg, bg)
        self.color_pairs[name] = curses.color_pair(self.color_pair_idx)
        self.color_pair_idx += 1

    def clrpair(self, name: str) -> int:
        global have_colors
        if not have_colors:
            return curses.A_NORMAL  # No color support
        return self.color_pairs[name]

    def resetpos(self) -> None:
        ymax, _ = self.screen.getmaxyx()
        self.ycur = ymax - 1
        self.screen.move(self.ycur, 0)

    def advance(self) -> None:
        self.ycur = max(-1, self.ycur - 1)
        if self.ycur >= 0:
            self.screen.move(self.ycur, 0)

    def write(self, msg: str, *attrs: int) -> None:
        if self.ycur < 0:
            return  # Past top
        self.screen.addstr(msg, *attrs)

    def writeline(self, msg: str, *attrs: int) -> None:
        self.write(msg, *attrs)
        self.advance()

    def append_message(self, **kwargs: Any) -> None:
        self.messages.append(kwargs)

    def refresh(self, input_only: bool = False) -> None:
        ymax, xmax = self.screen.getmaxyx()
        if input_only:
            self.screen.move(ymax - 1, 0)
            self.screen.clrtoeol()
            self.screen.move(ymax - 1, 0)
        else:
            self.screen.clear()
            self.resetpos()
        if self.input_enabled:
            input_line = self.input_buf
            if len(input_line) > xmax - 3:
                input_line = input_line[self.scroll_pos:]  # Scroll
                input_line = input_line[:xmax - 3]  # Truncate
            else:
                self.scroll_pos = 0
            input_line = '> {}'.format(input_line)
            self.writeline(input_line)
        else:
            input_line = ''
            self.writeline('(waiting for connection...)')
        if not input_only:
            for msg in reversed(self.messages):
                write_msgline(**msg)
                if self.ycur < 0:
                    break  # Past top
        self.screen.move(ymax - 1, 2 + self.input_pos - self.scroll_pos if input_line else 0)  # Input cursor
        self.screen.refresh()


curses_state: CursesState


def splitbymax(msg: str, maxl: int) -> Iterator[str]:
    global curses_state
    for i in count():
        line = msg[i*maxl:(i+1)*maxl:]
        if not line:
            break
        yield line


def splitmsg_inner(msg: str) -> Iterator[str]:
    global curses_state
    _, xmax = curses_state.screen.getmaxyx()
    for line in msg.splitlines():
        yield from splitbymax(line, xmax)


@overload
def splitmsg(msg: str, enum: Literal[False] = ...) -> Iterator[str]: ...


@overload
def splitmsg(msg: str, enum: Literal[True]) -> Iterator[Tuple[int, str]]: ...


def splitmsg(msg: str, enum: bool = False) -> Iterator[Any]:
    it: Iterator[Any] = splitmsg_inner(msg)
    if enum:
        it = enumerate(it)
    return reversed(tuple(it))


def write_colored_block(msg: str, color: str) -> None:
    global curses_state
    cpair = curses_state.clrpair(color)
    for line in splitmsg(msg):
        curses_state.writeline(line, cpair)


write_status = partial(write_colored_block, color='status')


def write_me(user: str, msg: str) -> None:
    write_colored_block('* {} {}'.format(user, msg), 'me')


def write_message(user: str, msg: str) -> None:
    global curses_state
    userpart = '[{}]'.format(user)
    msg = '{} {}'.format(userpart, msg)
    for i, line in splitmsg(msg, enum=True):
        if i == 0:
            curses_state.write(userpart, curses.A_BOLD)
            line = line[len(userpart):]
        curses_state.writeline(line)


MSG_FUNS: Dict[str, Callable[..., None]] = {
    'status': write_status,
    'message': write_message,
    'me': write_me,
}


def write_msgline(typ: str, **kwargs: Any) -> None:
    MSG_FUNS[typ](**kwargs)


# Minimal implementation of a sum of mutable sequences
class MultiSeqProxy:
    def __init__(self, subseqs: Sequence[MutableSequence[WaiterPair]]) -> None:
        self.subseqs = subseqs

    def append(self, value: AnyLock) -> None:
        for sub in self.subseqs:
            sub.append((value, self.subseqs))

    def remove(self, value: AnyLock) -> None:
        for sub in self.subseqs:
            sub.remove((value, self.subseqs))


# Hooks into methods used by threading.Condition.notify
class NotifierWaiters(WaiterSeq):
    def __iter__(self) -> Iterator[AnyLock]:
        return (value[0] for value in super().__iter__())

    # __getitem__ doesn't really take slices, but we need to define it as taking a slice to satisfy MutableSequence.
    @overload
    def __getitem__(self, index: int) -> AnyLock: ...
    @overload
    def __getitem__(self, s: slice) -> MutableSequence[AnyLock]: ...

    def __getitem__(self, index: Union[int, slice]) -> Any:
        assert isinstance(index, int)
        item: WaiterPair = super().__getitem__(index)
        return item[0]

    def remove(self, value: AnyLock) -> None:
        try:
            match: WaiterPair = next(x for x in super().__iter__() if x[0] == value)
        except StopIteration:
            raise ValueError('deque.remove(x): x not in deque')
        for ref in match[1]:
            try:
                super(NotifierWaiters, ref).remove(match)  # Remove waiter from known location
            except ValueError:
                raise RuntimeError('Unexpected missing waiter!')


# Supports waiting on multiple threading.Conditions objects simultaneously
class MultiCondition(threading.Condition):
    def __init__(self, lock: AnyLock) -> None:
        super().__init__(lock)

    def wait(self, children: Sequence[threading.Condition],  # type: ignore[override]
             timeout: Optional[float] = None) -> None:
        def get_waiters(c: threading.Condition) -> WaiterSeq:
            return c._waiters  # type: ignore[attr-defined]
        def set_waiters(c: threading.Condition, v: Union[WaiterSeq, MultiSeqProxy]) -> None:
            c._waiters = v  # type: ignore[attr-defined]
        def get_lock(c: threading.Condition) -> AnyLock:
            return c._lock  # type: ignore[attr-defined]

        assert len(frozenset(id(c) for c in children)) == len(children), 'Children must be unique'
        assert all(get_lock(c) is get_lock(self) for c in children), 'All locks must be the same'

        # Modify children so their notify methods do cleanup
        for child in children:
            if not isinstance(get_waiters(child), NotifierWaiters):
                set_waiters(child, NotifierWaiters((w, (get_waiters(child),))
                                                   for w in get_waiters(child)))
        set_waiters(self, MultiSeqProxy(tuple(get_waiters(c) for c in children)))

        super().wait(timeout)

    def notify(self, n: int = 1) -> NoReturn:
        raise NotImplementedError

    def notify_all(self) -> NoReturn:
        raise NotImplementedError

    notifyAll = notify_all  # noqa: N815


if TYPE_CHECKING:
    class GenericQueue(queue.Queue[T], Generic[T]):
        pass
else:
    T = None

    class FakeGenericMeta(type):
        def __getitem__(cls, item: Any) -> FakeGenericMeta:
            return cls

    class GenericQueue(queue.Queue, metaclass=FakeGenericMeta):
        pass


class LockedQueue(GenericQueue[T]):
    def __init__(self, lock: AnyLock, maxsize: int = 0) -> None:
        super().__init__(maxsize)
        self.mutex = lock  # type: ignore[assignment]
        self.not_empty = threading.Condition(lock)
        self.not_full = threading.Condition(lock)
        self.all_tasks_done = threading.Condition(lock)


class KeyErrorMessage(str):
    def __repr__(self) -> str:
        return self


class ExceptionWrapper:
    """Wraps an exception plus traceback to communicate across threads."""

    def __init__(self, exc_info: Optional[OptExcInfo] = None, where: str = 'in background') -> None:
        # It is important that we don't store exc_info
        if exc_info is None:
            exc_info = sys.exc_info()
        assert exc_info[0] is not None
        self.exc_type = exc_info[0]
        self.exc_args = exc_info[1].args
        self.exc_msg = ''.join(traceback.format_exception(*exc_info))
        self.where = where

    def reraise(self) -> NoReturn:
        """Reraises the wrapped exception in the current thread."""
        # Format a message such as: "Caught ValueError in DataLoader worker
        # process 2. Original Traceback:", followed by the traceback.
        msg = 'Caught {} {}.\nOriginal {}'.format(
            self.exc_type.__name__, self.where, self.exc_msg)
        if self.exc_type == KeyError:
            # KeyError calls repr() on its argument (usually a dict key). This
            # makes stack traces unreadable. It will not be changed in Python
            # (https://bugs.python.org/issue2651), so we work around it.
            msg = KeyErrorMessage(msg)
        exc = self.exc_type(msg)
        exc.orig_args = self.exc_args  # type: ignore[attr-defined]
        raise exc


class AsyncCallable(Generic[Response]):
    def __init__(self, lock: AnyLock, fun: Callable[..., Response], name: Optional[str] = None) -> None:
        self.lock = lock
        self.fun = fun
        if TYPE_CHECKING:
            Params = Tuple[Tuple[Any, ...], Dict[str, Any]]  # (args, kwargs)
        self.request: LockedQueue[Optional[Params]] = LockedQueue(lock, maxsize=1)
        self.response: LockedQueue[Union[Response, ExceptionWrapper]] = LockedQueue(lock, maxsize=1)
        self.quit_flag = False
        self.thread = threading.Thread(target=self.run_thread, name=name, daemon=True)
        self.thread.start()

    def run_thread(self) -> None:
        while not self.quit_flag:
            request = self.request.get()
            if request is None:
                break  # quit sentinel
            args, kwargs = request
            try:
                response: Union[Response, ExceptionWrapper] = self.fun(*args, **kwargs)
            except Exception:
                response = ExceptionWrapper(where='in AsyncCallable(name={!r})'.format(threading.current_thread().name))
            self.response.put(response)

    def put(self, *args: Any, **kwargs: Any) -> None:
        self.request.put((args, kwargs))

    def get(self, *args: Any, **kwargs: Any) -> Response:
        msg = self.response.get(*args, **kwargs)
        if isinstance(msg, ExceptionWrapper):
            msg.reraise()
        return msg


class UnknownUser:
    def __str__(self) -> str:
        return '<unknown>'


@dataclass
class ConnData:
    luser: Union[str, UnknownUser] = UnknownUser()  # Local user
    ruser: Union[str, UnknownUser] = UnknownUser()  # Remote user


def process_packet(packet: Dict[str, Any], conn_data: ConnData, remote: bool = False) -> None:
    global curses_state
    packet_user = conn_data.ruser if remote else conn_data.luser

    def badpacket(msg: str) -> None:
        curses_state.append_message(typ='status', msg='Bad packet from {}: {}'.format(packet_user, msg))

    def parse_field(obj: Dict[str, Any], key: str, name: str, typ: Type[T], choices: Optional[Container[T]] = None) \
            -> Optional[T]:
        if (value_ := obj.get(key)) is None:
            badpacket('Missing key: {}'.format(name))
            return None
        if not isinstance(value_, typ):
            badpacket('{} has wrong type: Expected {}, got {}'.format(name, typ.__name__, type(value_).__name__))
            return None
        value = value_
        if choices is not None and value not in choices:
            badpacket('{} has unknown value: {}'.format(name, value))
            return None
        return value

    ptype = parse_field(packet, 'type', 'packet.type', str, choices=('message', 'username', 'me'))
    if ptype is None:
        return
    if ptype == 'message':
        msg = parse_field(packet, 'message', 'packet.message', str)
        if msg is None:
            return
        curses_state.append_message(typ='message', user=packet_user, msg=msg)
    elif ptype == 'username':
        username = parse_field(packet, 'username', 'packet.username', str)
        if username is None:
            return
        if not isinstance(packet_user, UnknownUser):
            curses_state.append_message(typ='status', msg='{} is now known as {}'.format(packet_user, username))
        elif remote:
            curses_state.append_message(typ='status', msg='Say hello to {}!'.format(username))
        if remote:
            conn_data.ruser = username
        else:
            conn_data.luser = username
    elif ptype == 'me':
        msg = parse_field(packet, 'message', 'packet.message', str)
        if msg is None:
            return
        curses_state.append_message(typ='me', user=packet_user, msg=msg)
    else:
        raise AssertionError('Invalid packet type')


def send(sock: socket.socket, packet: Dict[str, Any]) -> None:
    sock.send(json.dumps(packet).encode('ascii'))


def make_packet(msg: str) -> Optional[Dict[str, Any]]:
    # Craft packet based on input
    packet: Optional[Dict[str, Any]]
    if not msg.startswith('/'):
        # Message
        packet = dict(type='message', message=msg)
    else:
        # Command
        args = msg.rstrip('\n').split(' ')
        if args[0] in ('/help', '/?', '/h'):
            curses_state.append_message(
                typ='status',
                msg=(
                    'Commands:\n'
                    '/help               Show this help\n'
                    '/nick <username>    Set your nickname\n'
                    '/me <message>       Send a "me" message\n'
                    '/quit               Quit this program'
                ),
            )
            packet = None
        elif args[0] == '/nick':
            if len(args) == 2:
                packet = dict(type='username', username=args[1])
            else:
                curses_state.append_message(
                    typ='status',
                    msg='Wrong number of arguments for /nick: Expected {}, got {}'.format(1, len(args) - 1),
                )
                packet = None
        elif args[0] == '/me':
            packet = dict(type='me', message=' '.join(args[1:]))
        elif args[0] == '/quit':
            sys.exit(0)
        else:
            curses_state.append_message(typ='status', msg='Unknown command: {}'.format(args[0]))
            packet = None
    return packet


def process_input(sock: socket.socket, stdin_thread: AsyncCallable[int], net_thread: AsyncCallable[str],
                  conn_data: ConnData, remote: str) -> bool:
    if stdin_thread.response.qsize():
        # Read input
        new_message = False
        try:
            key = stdin_thread.get(block=False)
        except OSError as e:
            if (
                os.name == 'nt'
                and type(e) is OSError
                and hasattr(e, 'orig_args')
                and e.orig_args[0] == errno.EBADF  # type: ignore[attr-defined]
            ):
                raise EOFError  # End of input, exit loop
            raise

        if 0x20 <= key < 0x7F:  # Printable ASCII characters
            c = chr(key)
            ipos = curses_state.input_pos
            curses_state.input_buf = curses_state.input_buf[:ipos] + c + curses_state.input_buf[ipos:]
            curses_state.input_pos += 1
            _, xmax = curses_state.screen.getmaxyx()
            if 2 + curses_state.input_pos - curses_state.scroll_pos == xmax:
                curses_state.scroll_pos += 1
        elif key in (ord('\b'), 0x7F):  # Backspace/DEL
            ipos = curses_state.input_pos
            if ipos > 0:
                _, xmax = curses_state.screen.getmaxyx()
                if 2 + len(curses_state.input_buf) - curses_state.scroll_pos == xmax - 1:
                    # Last character is visible and will move left in buffer, reduce scroll
                    curses_state.scroll_pos = max(0, curses_state.scroll_pos - 1)
                curses_state.input_buf = curses_state.input_buf[:ipos-1] + curses_state.input_buf[ipos:]
                curses_state.input_pos -= 1
        elif key == curses.KEY_DC:  # Delete key
            ipos = curses_state.input_pos
            if ipos < len(curses_state.input_buf):
                _, xmax = curses_state.screen.getmaxyx()
                if 2 + len(curses_state.input_buf) - curses_state.scroll_pos == xmax - 1:
                    # Last character is visible and will move left in buffer, reduce scroll
                    curses_state.scroll_pos = max(0, curses_state.scroll_pos - 1)
                curses_state.input_buf = curses_state.input_buf[:ipos] + curses_state.input_buf[ipos+1:]
        elif key == curses.KEY_LEFT:
            curses_state.input_pos = max(curses_state.input_pos - 1, 0)
            if curses_state.scroll_pos > curses_state.input_pos:
                curses_state.scroll_pos = curses_state.input_pos
        elif key == curses.KEY_RIGHT:
            if curses_state.input_pos < len(curses_state.input_buf):
                _, xmax = curses_state.screen.getmaxyx()
                curses_state.input_pos += 1
                if 2 + curses_state.input_pos - curses_state.scroll_pos == xmax:
                    curses_state.scroll_pos += 1
        elif key == curses.KEY_HOME:
            curses_state.input_pos = 0
            curses_state.scroll_pos = 0
        elif key == curses.KEY_END:
            _, xmax = curses_state.screen.getmaxyx()
            curses_state.input_pos = len(curses_state.input_buf)
            curses_state.scroll_pos = 2 + len(curses_state.input_buf) - (xmax - 1)
        elif key == ord('\n'):  # Line feed
            msg = curses_state.input_buf
            curses_state.input_buf = ''
            curses_state.input_pos = 0

            if msg:
                packet = make_packet(msg)
                new_message = True
            else:
                packet = None

            # Send the packet
            if packet is not None:
                # Process packet locally
                process_packet(packet, conn_data)
                # Send packet to remote
                try:
                    send(sock, packet)
                except OSError:
                    return False  # Socket error, exit loop

        curses_state.refresh(input_only=not new_message)
        stdin_thread.put()  # Let it run again

    if net_thread.response.qsize():
        # Receive packet from remote
        try:
            packet_data = net_thread.get(block=False)
        except OSError:
            return False  # Socket error, exit loop

        if not packet_data:
            sock.shutdown(socket.SHUT_RD)
            curses_state.append_message(typ='status', msg='Connection terminated by {}'.format(remote))
            curses_state.refresh()
            return False  # Socket closed, exit loop

        try:
            packet_in = json.loads(packet_data)
        except JSONDecodeError:
            curses_state.append_message(
                typ='status', msg="Bad packet from {}: Invalid JSON: '{}'".format(conn_data.ruser, packet_data))
        else:
            process_packet(packet_in, conn_data, remote=True)

        curses_state.refresh()
        net_thread.put()  # Let it run again

    return True  # Continue loop


def readwrite(sock: socket.socket, lock: AnyLock, stdin_thread: AsyncCallable[int], remote: str,
              username: Optional[str] = None) -> None:
    if username is None:
        username = 'client' if remote == 'server' else 'server'

    # Network thread
    net_thread = AsyncCallable(lock, lambda: sock.recv(4096).decode('utf-8'), 'Network Thread')
    net_thread.put()

    # Connection data
    conn_data = ConnData()

    # Setup username
    user_packet = dict(type='username', username=username)
    process_packet(user_packet, conn_data)
    send(sock, user_packet)
    del user_packet

    # I/O loop
    multicond = MultiCondition(lock)
    try:
        with multicond:
            while True:
                if not process_input(sock, stdin_thread, net_thread, conn_data, remote):
                    break

                # All conditions false, wait for a change
                multicond.wait((stdin_thread.response.not_empty, net_thread.response.not_empty))
    finally:
        net_thread.quit_flag = True


open_sockets: List[socket.socket] = []


if os.name == 'nt':
    @ctypes.WINFUNCTYPE(wintypes.BOOL, wintypes.DWORD)  # type: ignore[attr-defined,misc]
    def console_ctrl_handler(event: int) -> bool:
        if event == 2:  # CTRL_CLOSE_EVENT
            for sock in open_sockets:
                sock.shutdown(socket.SHUT_RDWR)
                sock.close()
            open_sockets.clear()
        return False  # Chain to next handler


def setup_exit_handlers() -> None:
    # Raises SystemExit to terminate gracefully
    def handle_term_signal(signum: Signals, frame: FrameType) -> None:
        if not sys.is_finalizing():
            sys.exit(1)
    signal.signal(signal.SIGTERM, handle_term_signal)
    if hasattr(signal, 'SIGHUP'):
        signal.signal(signal.SIGHUP, handle_term_signal)

    if os.name == 'nt':
        kernel32 = ctypes.WinDLL('kernel32', use_last_error=True)  # type: ignore[attr-defined]
        if not kernel32.SetConsoleCtrlHandler(console_ctrl_handler, True):
            raise ctypes.WinError(ctypes.get_last_error())  # type: ignore[attr-defined]


def setup_inthread(lock: AnyLock) -> AsyncCallable[int]:
    global curses_state
    stdin_thread = AsyncCallable(lock, curses_state.screen.getch, 'Input Thread')
    stdin_thread.put()
    return stdin_thread


def main(stdscr: _CursesWindow) -> None:
    global curses_state
    curses_state = CursesState(stdscr)

    for color in COLORS:
        curses_state.make_color_pair(*color)

    parser = argparse.ArgumentParser()
    parser.add_argument('-l', dest='listen', action='store_true',
                        help='Listen mode, for inbound connects')
    parser.add_argument('-k', dest='keep_open', action='store_true',
                        help='Keep inbound sockets open for multiple connects')
    parser.add_argument('-u', dest='username',
                        help='Username')
    parser.add_argument('destination', nargs='?')
    parser.add_argument('port', nargs='?')
    options = parser.parse_args()

    if not options.listen and options.keep_open:
        parser.error('must use -l with -k')
    if options.username is None:
        options.username = 'server' if options.listen else 'client'
    if options.port is None and options.destination is not None:
        options.port, options.destination = options.destination, options.port  # Swap so port takes precedence

    main_thread_lock = threading.RLock()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Open TCP socket
    open_sockets.append(sock)
    try:
        if options.listen:
            # Server code
            if options.port is None:
                raise ValueError('Missing port number')
            dest = '' if options.destination is None else options.destination
            sport = int(options.port)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            if hasattr(socket, 'SO_REUSEPORT'):
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
            sock.bind((dest, sport))
            sock.listen(1)
            curses_state.append_message(typ='status', msg='Listening on {}:{}'.format(dest or '0.0.0.0', sport))
            curses_state.refresh()

            stdin_thread: Optional[AsyncCallable[int]] = None

            # Server loop
            while True:
                try:
                    (csock, caddr) = sock.accept()
                except KeyboardInterrupt:
                    break  # Exit loop

                open_sockets.append(csock)
                try:
                    (chost, cport) = socket.getnameinfo(caddr, 0)
                    curses_state.append_message(typ='status', msg='Connection received on {}:{}'.format(chost, cport))
                    curses_state.input_enabled = True
                    curses_state.refresh()

                    if stdin_thread is None:
                        stdin_thread = setup_inthread(main_thread_lock)

                    try:
                        readwrite(csock, main_thread_lock, stdin_thread, 'client', options.username)
                    except (EOFError, KeyboardInterrupt):
                        break  # Exit loop

                    curses_state.input_enabled = False
                    curses_state.refresh()
                finally:
                    csock.close()
                    open_sockets.remove(csock)
                if not options.keep_open:
                    break  # Do not loop
        else:
            # Client code
            if options.destination is None:
                raise ValueError('Missing destination')
            if options.port is None:
                raise ValueError('Missing port number')
            dest, port = options.destination, int(options.port)
            sock.connect((dest, port))
            curses_state.append_message(typ='status', msg='Connected to {}:{}'.format(dest, port))
            curses_state.input_enabled = True
            curses_state.refresh()

            stdin_thread = setup_inthread(main_thread_lock)

            # Client loop
            try:
                readwrite(sock, main_thread_lock, stdin_thread, 'server', options.username)
            except (EOFError, KeyboardInterrupt):
                pass  # Exit loop
    finally:
        sock.close()
        open_sockets.remove(sock)


if __name__ == '__main__':
    setup_exit_handlers()

    for fd, name in ((0, 'stdin'), (1, 'stdout'), (2, 'stderr')):
        try:
            os.fstat(fd)
        except OSError as e:
            if e.errno == errno.EBADF:
                raise RuntimeError('cannot use curses because {} is closed'.format(name))
            raise

    if not os.isatty(1):
        raise RuntimeError('cannot use curses because stdout is not a tty')

    stdscr = curses.initscr()
    try:
        if os.name == 'posix':
            try:
                curses.start_color()  # Enable color
            except curses.error:
                pass  # No color is fine
            else:
                if curses.has_colors():
                    have_colors = True
                    curses.use_default_colors()  # Use terminal's background color
        curses.noecho()  # Disable echoing of input
        curses.cbreak()  # Input without pressing enter
        stdscr.keypad(True)  # Read special keys as single codepoints
        main(stdscr)
    finally:
        curses.echo()
        curses.nocbreak()
        if termios is not None:
            # Clean up after initscr()
            attr = termios.tcgetattr(1)
            attr[1] |= termios.ONLCR  # type: ignore[operator]
            attr[3] |= termios.ECHO  # type: ignore[operator]
            termios.tcsetattr(1, termios.TCSADRAIN, attr)
        if os.name == 'posix':
            print('\x1bc', end='')  # Clear screen
